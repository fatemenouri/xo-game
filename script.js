const statusDisplay = document.querySelector('.status');
let gameActive = true;
let currentPlayer = "X";
let states = ["", "", "", "", "", "", "", "", ""];
const playerTurn = () => `${currentPlayer}'s turn`;

statusDisplay.innerHTML = playerTurn();


function cellPlayedHandler(clickedCell, Id) {

    states[Id] = currentPlayer;
    clickedCell.innerHTML = currentPlayer;
}
function playerCheck() {
    currentPlayer = (currentPlayer === "X") ? "O" : "X";
    statusDisplay.innerHTML = playerTurn();
}
function clickHandler(clickedCellEvent) {

    const clickedCell = clickedCellEvent.target;

    const Id = parseInt(clickedCell.getAttribute('id'));

    if (states[Id] !== "" || !gameActive) {
        return;
    }

    cellPlayedHandler(clickedCell, Id);
    checkWinner();

}

const winningCells = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];
function checkWinner() {
    let isWin = false;
    for (let i = 0; i <= 7; i++) {
        const cells = winningCells[i];
        let a = states[cells[0]];
        let b = states[cells[1]];
        let c = states[cells[2]];

        if (a === '' || b === '' || c === '') {
            continue;
        }
        if (a === b && b === c) {
            isWin = true;
            break
        }
    }
    if (isWin) {
        statusDisplay.innerHTML = `${currentPlayer} is winner!`;
        gameActive = false;
        return;
    }
    let isDraw = !states.includes("");
    if (isDraw) {
        statusDisplay.innerHTML = `the game is a draw!`;
        gameActive = false;
        return;
    }
    playerCheck();
}

function restart() {
    gameActive = true;
    currentPlayer = "X";
    states = ["", "", "", "", "", "", "", "", ""];
    statusDisplay.innerHTML = playerTurn();
    document.querySelectorAll('.cell').forEach(cell => cell.innerHTML = "");
}

document.querySelectorAll('.cell').forEach(cell => cell.addEventListener('click', clickHandler));
document.querySelector('.restart').addEventListener('click', restart);